# [`QA Box`] QA Automation Skills

![Main Logo](../../assets/logo-furman(100).png){ align=right }

## Knowledge, workflows & skills

- Bug reporting and bug tracking require a specific set of skills which are quite different from those needed in other areas such as design or development.
- The reason QA requires a special kind of profile is that it’s the one part that sits right in the middle of the software development matrix, working with and among developers, designers, product managers, users & clients.
- If you think about the role of QA and bug reporting, you can think of a middle man. The middle man is the one in touch with alpha and beta testers and customers. People in QA and testing are the ones that have to tell the web developer to change (fix) their code.
- And when the situation calls for it, they are also the ones to tell the web designers that their design isn’t intuitive enough for the end users.
- They communicate with the product team and make sure they are on track to fix the existing bugs. All these people have different agendas and it is up to the QA agent to make sure that everyone is aligned and knows what needs to be done.
- QA testers need to speak more than one language. They basically have to understand the workings behind the entire product (to some extent).

![Main Logo](../../assets/logo-furman(50).png){ align=right }

### 01. Tech skills

- As a tester, you are hired to press certain software or products hard. You’re the one who’s following every little step, every little hint. You’re chasing down bugs. You’re Horatio Caine

![Main Logo](../../assets/logo-furman(50).png){ align=right }

### 02. Good at communication

- When a QA finally has some information, he or she has to be willing to ask developers questions when questions need to be asked.

![Main Logo](../../assets/logo-furman(50).png){ align=right }

### 03. Diplomatic

- Looking for other people’s mistakes and bringing them to light is always a tough thing to do. Doing it as a tester or bug reporter means that you must be able to work together with these people in an efficient manner. It also means being able to get the job done even if it means having to deal with a bit of resistance and crankiness here and there.

- No one likes bugs and no one likes them to be found. Establishing an open-minded culture of trial & error is absolutely key here.

- It doesn’t help to point fingers at your developers for certain bugs or mistakes. It simply doesn’t work.

![Main Logo](../../assets/logo-furman(50).png){ align=right }

### 04. Negotiator

- In order to get things done in a reasonable time, negotiation skills are crucial. Here’s why.
      - The development team is busy with their development work, so they’ll have to make time to fix bugs.
      - That means there will be a whole lot of back and forth between QA and development on when and what should be done. This is less the case with small issues but can be an important factor with larger issues.

![Main Logo](../../assets/logo-furman(50).png){ align=right }

### 05. Good at testing

- This is your thing. You absolutely need to be good at testing. Having a good understanding of different testing concepts, workflows, tools, and techniques are a must-have.

- If you don’t have that, you’re not the right person for this role.

![Main Logo](../../assets/logo-furman(50).png){ align=right }

### 06. Deep diver

- When you’re dealing with bugs, thinking you understand the issue is never as good as really understanding what’s happening. If you don’t, your instructions are bound to be vague and will, without a doubt, result in an increase of the costs.

- To have a certain level of curiosity.

- As a bug reporter, you spent a lot of time on the product itself and you’re learning the ins and outs of the product. Be prepared to dive deep into your product and its technological basis.

![Main Logo](../../assets/logo-furman(50).png){ align=right }

### 07. Understand your product from your end-users perspective

- The software has to fulfill a certain function. Understanding the product requirements and how they help the users is key to being able to communicate well with everyone. It’s especially key to be able to communicate with the end user.

- QA has to understand how the product works from an end user’s point of view and what he or she was trying to do. It will also help in distinguishing the critical issues from the not-so-critical ones.

## The QA unicorn

- Technogogy skills
- Domain expertise
- Communication and business skills

### Domain expertise

- In today's fast paced tech landscape, companies seek out teams with specialized, domain-specific knowledge when working with a QA partner. Whether your product serves the legal, financial, CRM, social media, healthcare, or manufacturing space, having a team that knows the intricacies of your domain ensures that your users are satisfied.

- Rich domain knowledge is valuable for many reasons, but here are the top 5 as listed by our expert QA engineers:

- It helps your team speak the language

      - If there is an error in a particular transaction process in a financial application, the engineer will be able to accurately describe the problem and its impact on the product as a whole.

- It puts your team in the user's shoes

      - Domain knowledge makes it easier for a QA engineer to view the product through the eyes of the user. This means that defects in the graphical user interface (GUI) will be uncovered in the initial stage of testing instead of later, when the product is nearing release or is already delivered to customers.

- It boosts the productivity of your team

      - QA engineers with extensive domain knowledge are in high demand because their experience allows them to go far beyond the discovery of a bug – they can suggest and implement solutions. Conversely, an engineer with only technical skills would have to relay the issue to someone else, wasting both time and budget.

- It tells the team where to look for defects

      - The processing structure and architecture of a product appear clearly to a QA engineer with previous experience in the domain, resulting in more thorough testing and the ability to look for common yet easily missed defects. Specific data sets and code are more quickly understood and validated when the engineer has worked with them before.

- It helps teams prioritize bugs

      - If the engineer has worked in the domain before, they will know how to categorize bugs more efficiently. By prioritizing bugs that are critical to the system's functionality, the engineer effectively manages time and cost while also improving and refining the product.
