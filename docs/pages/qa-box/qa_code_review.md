
# [`QA Box`] Code Review

The primary goal of the code review process is to assess any new code for bugs, errors, and quality standards set by the organization. Benefit of the code review process is the collective team’s improved coding skills.

Code review is a very important phenomenon in the SOFTWARE development process. Made popular by the open-source community, it is now the standard for any team of developers. If it is executed correctly, the benefit is not only in reducing the number of bugs and better code quality but also in the training effect for the programmer.

Code review is critical for the following reasons:

- Ensure that you have no bugs in the code.
- Minimize your chances of having issues.
- Confirm new code adheres to guidelines.
- Increase the efficiency of the new code.

Code reviews further lead to improving other team members' expertise. As a senior developer typically conducts a code review, a junior developer may use this feedback to improve their own coding.

## Code review levels

1. Typos and problems with styles (this is the first thing that catches the eye with a cursory code review). This item should be excluded from the code review process using automation.

2. Problems with names, functions, classes, folders, and files.

3. The location of the code in the project i.e. whether this function is located in the desired file, class. Whether this class is located in the right folder in the right place in your project.

4. And whether this code is necessary at all. Perhaps this function is already implemented, but people just do not know about its existence. It may be much better to use some well-known library that solves this problem instead of writing this new code.

5. Algorithms and approaches. Reviewed as a principle your approach fits into the system. Criticize the selected algorithm or approach. They are trying to understand a more general picture of what is happening.

<!-- ## The list of shortcomings that you should pay attention to during code review -->

## Shortcomings - pay attention to during code review

`No Proof of Success.` The automation scripts need to run successfully in order to pass review, and proof of success (such as a log or a screenshot) must be attached to the review.

`Typos and Bad Formatting.` Typos and bad formatting reflect carelessness, cause frustration, and damage reputation. They are especially bad for Behavior-Driven Development frameworks.

`Hard-Coded Values.` Hard-coded values often indicate hasty development. Sometimes, they aren’t a big problem, but they can cripple an automation code base’s flexibility.

`Incorrect Test Coverage.` It is surprisingly common to see an automation script that doesn’t actually cover the intended test steps. A step from the test procedure may be missing, or an assertion may yield a false positive. Sometimes, assertions may not even be performed! When reviewing tests, keep the original test procedure handy, and watch out for missing coverage.

`Unclear Comments.` Leaving unclear comments to the author for any changes needed is the most reviewers make. It is necessary to put in clear comments to create space for dialogue.

`Poor Code Placement.` Automation projects tend to grow fast. Along with new tests, new shared code like page objects and data models are added all the time. Maintaining a good, organized structure is necessary for project scalability and teamwork. Test cases should be organized by feature area. Common code should be abstracted from test cases and put into shared libraries. Framework-level code for things like inputs and logging should be separated from test-level code. If the code is put in the wrong place, it could be difficult to find or reuse. It could also create a dependency nightmare. For example, non-web tests should not have a dependency on Selenium WebDriver. Make sure the new code is put in the right place.

`Bad Config Changes.` As a general rule, submit any config changes in a separate code review from other changes, and provide a thorough explanation to the reviewers for why the change is needed.

`Brittleness.` Here are a few examples of brittleness to watch out for in review:

1. Do test cases have adequate cleanup routines, even when they crash?
2. Are all exceptions handled properly, even unexpected ones?
3. Is Selenium WebDriver always disposed of?
4. Will SSH connections be automatically reconnected if dropped?
5. Are XPaths too loose or too strict?
6. Is a REST API response code of 201 just as good as 200?

`Ignoring Design.` Many reviewers ignore the overall design and just focus on the functionality and workability of the automation script. But by doing this, they miss the role of the automation script in the whole system and how it fits into what is already there.

`Duplication` is the biggest problem for test automation. Many testing operations are inherently repetitive. Engineers sometimes just copy-paste code blocks, rather than seek existing methods or add new helpers, to save development time. Plus, it can be difficult to find reusable parts that meet immediate needs in a large codebase. Nevertheless, good code reviews should catch code redundancy and suggest better solutions.

## Conclusion

The code review stage is highly underestimated in the testing and quality assurance process. If you are a tester who works within the framework of iterative development, familiarization with pull requests should be an integral part of your work after familiarization with the requirements. On the other hand, your code should also be viewed by other team members, which is a key factor in mastering test automation.
