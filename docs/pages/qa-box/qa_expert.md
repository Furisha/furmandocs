# [`QA Box`] QA Expert

"All Humans are testers" - James Bach

## Level of learning

- Level 0: “I overcome obliviousness” - I now realize there is something here to learn
- Level 1: “I overcome intimidation” - I feel I can learn this subject or skill. I knew enough about it so that I am not intimidated by people who knew more than me
- Level 2: “I overcome incoherence” - I no longer feel that I’m pretending or hand-waving. I feel reasonably competent to discuss or practice. What I say sounds like I think I know
- Level 3: “I overcome competence” - Now I feel productively self-critical, rather than complacently good enough. I want to take the risk, invent, teach, and push myself. I want to be with other enthusiastic people.

## Are You A Testing Expert

Analyze this claims:

- You should write a test plan
- It’s important that testing can be repeatable
- Fach test case should have an expected results
- Test automation save money and time
- God enough quality is not good enough
- An undocumented test cannot be improved
- Exploratory testing is a useful practice
- It’s better to use term “defect” than ‘Bug”
- Ambiguity. should be removed from requirements

### Experts vs. Non-Experts

Non-Experts are more likely to say:

- "Yes, thats what the book say."
- "That is right"
- "That is wrong"
- "I don’t know"

Experts are more likely to say:

- "Tell me more about the content?
- "I can think of how that might be true and I can think of how it might be false. Let’s think it through"
- "Let me refresh that"
- "Here’s some possible answers"
- "Here’s one way I’ve solved this"
- "I don’t know. Here’s how I will find out."

### Perfect Testing

The infinite process of comparing the invisible to the ambiguous so as to avoid the unthinkable happening to the anonymous.
In other words, perfect testing is a challenge.

Testing is a complex problem-solving activity.

Learning testing on your own doesn’t  cost you much, you don’t need anyone’s permission.

However, it’s hard to know if you are doing it well. Good testing varies a lot with the context.

### Context-Driven Testing Community Principles

- The value of any practice depends on its context.
- There are good practices in context, but there are no best practices.
- People, working together, are the most important part of the any project’s context.
- Project unfold over time in ways that are often not predictable
- The product is solution. If the problem isn’t solved, the product doesn’t work.
- Good software testing is a challenging intellectual process.
- Only through judgement and skills, exercised cooperatively throughout the entire project, are we able to do the right things at the right times to effectively test our product.

### Some Cool Things That Experts Have And Do

Experts have:

- Situational awareness
- Confidence in confusion
- Colleague network
- Trained reflexes
- Awareness of limitations
- Diverse (different) experiences
- Relevant knowledge
- Mental models for problem solving (Schemata)
- Reputation

Experts do:

- Avoid traps and dead ends
- Systematic inquiry
- Comfort authority and convention
- Self-training and retraining
- Self-criticism
- Pattern matching on experience
- Coherent methodology
- Write, speak, teach

#### Situational awareness

- How to ask questions
- What kind of project situation are we in?
- How much time do we have?
- Who is involved?
- Who’s on the test team?
- What kind of deliverable does it have to be?
- Who’s our customer
- What information do I have on my desk right now about this problem
- What informations is available via the various electronic mechanisms

#### Confidence in confusion

- If I’m confused, I’m confident

#### Colleague network

Colleague: I’m not good at all kind of testing. So I call colleague, he/she knows better than I, and he/she will transfer that skill to me

### Personal Vision Of Testing Expertise

- I can test anything under any conditions in any time frame
- I deliver useful results in a usable form
- I perform at least as well as any other expert would
- I choose methods that fit the situation
- I can explain and defend my work on demand
- I collaborate effectively with the project team
- I make appropriate use of available tools and resources
- I advise clients about the risks and limitations of my work
- I advise clients about how they can help me to do better work
- I faithfully and ethically serve my clients

### Rapid Testing

Is a mind-set and a skill-set of testing focused on how to do testing more quickly, less expensively, with excellent results. This is general testing methodology. It’s adapts to any kind of project or product.

#### Definition of Rapid Software Testing

Evaluate by learning through experiencing, exploring, end experimenting including questioning, modelling, observation, inference, study, etc.

Thomas Gray was musing about innocence as he watched school boys playing in a yard. He was thinking, “Don’t break the spell. Let them be blissfully happy kids.”. We, however, are not children.

- We should not wait until we are sure there is trouble.
- We must have faith in trouble; assume there is trouble until we can prove there isn’t.
- This helps us in our projects, in our testing, and it helps keep our families safe.

### Invest In Testability

The success of any tooling strategy depends in large part on how well your technology affords interactions with tools. This is why it pays to build testability into your products. From a tool perspective this means two big things: that your product is controllable by tools via easily scripted interfaces, and that its states and outputs are observable by those tools. For this reason, browser based products tend to be more testable while apps on mobile devices are less testable. Products with standard controls are more testable than products with custom controls. Consider making testability review a part of each iteration, and otherwise install this thought process early in your projects. Any serious attempt to make tooling work must go hand-in-hand with testable engineering.
