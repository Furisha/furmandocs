# [`QA Box`] Coding Guidelines

Automated testing is no longer an option, it’s a must-have in every tester’s bag of tricks. With software being rolled out faster to meet ever-increasing customer demands and competition, test automation will continue to be on the rise. Testing early and fast helps identify bugs faster, saves time, effort and costs to a large extent.

I have come across and followed some useful coding guidelines which I feel every QA automation tester should follow during test automation script creation. Hope you people find it helpful.

## Programming

- Follow Programming Language Guidelines (method name-getPayload(), class name- APIEndpoints, package name-com.testingsumo.backend.tests, variable name-authToken)
- Follow Oops Concepts Wherever Possible - Abstraction(base classes), Inheritance(multiple implementations of same things/multiple inheritances), Polymorphism(many forms with something different), Data Hiding(hide unnecessary/sensitive info), Encapsulation(Bind small entities into a single large entity)
- Write clean code
      - Keep it simple. Always look over your code and ask yourself if there's a simpler way of doing what it is that you are trying to do. Sometimes the obvious solution to a testing problem only becomes clear after you have solved it in a complicated way; now it's time to go back and solve it more elegantly.
      - Don't repeat yourself. If there's something you're doing in more than one test - for example, logging in to the application - write a method that you can call instead of putting those steps into every test. Similarly, create a file where you save all of your variables and element locators, and have all of your tests refer to that file. That way if a variable or a locator changes, you can make the change in one place rather than several.
      - Be consistent. Consistent code is easier to read. Be consistent with your casing: if you have a variable for the user's first name called “firstName”, don't make the variable for the user's last name "LastName".  Follow the conventions that your developers are using: if they indent with two spaces, you should too. If they put their opening curly braces on a separate line, you should as well.
      - Comment your code. It's not always obvious what test automation code is doing at first glance, and while you might be quite used to the syntax you are using for your tests, your developers might not be familiar with it. Simple comments like "Polling the queue for the delete request" can be really helpful in explaining your intent. Moreover, what might seem really obvious to you now might not be obvious in three months when you need to update the test! Your future self will thank you for the comments you write today.
- Reduce code duplication (think before writing new code, can I use/make changes in existing code?)
- Increase code reusability
- Make your code generic wherever possible
- Leave no hardcoded data in source code
- Keep your static data outside the source code
- Keep your dynamic data dynamic in test code (fetch it from DB queries or scripts)
- Test your code properly, use IDE options such as call hierarchy or show usage to test your changes end 2 end

## Framework and Debugging

- Use Extensive logging- everything which is part of source code should be analyzed from logs without looking at the source code
- Generate and save failure proofs outside the src code- videos/data/screenshots/logs
- Focus on making your code scalable and faster without compromising the code quality
- Your code should be platform and system independent
- Use as many assertions as possible focus on automated testing rather than automation
- Leave no hardcoded data in source code
- Always think for the future, separate out tech dependencies so that migration to new tech is easy in case it is needed
- Keep your tests independent for better results in multithreading unless they are related (for example publisher-subscriber related tests)
- Use Proper Documentation
- Create code that can be easily read and modified by others