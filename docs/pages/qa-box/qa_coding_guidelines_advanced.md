# [`QA Box`] Advanced Coding Guidelines

## Purpose

The purpose of this document is to document and follow Naming conventions and guidelines during coding framework designing for Test Automation project.

## 01. Naming Conventions

- File names should be in lower case and each word separated by `_`. The name should self explanatory. e.g `project_test_login.spec.js`, or if using Python `project_test_login.py`
- Data / Document files :  Camelcase and name should self explanatory. e.g `Project_Automation_README.do`, or `Project_Automation_README.md`
- Class names should follow the UpperCaseCamelCase convention. e.g. class `LowLevelLib`, or `BaseClass`, `LoginStepDefinitions`
- Variable names should be in lower case and should be self explanatory e.g `single_textbox_question`, or `standard_user`
- Function names should start with lowercase word and then Camelcase should be followed. It should be self explanatory e.g. `fillQuestionAndAnswer`, or `clickOnLoginButton`

## 02. Project Organization

- Organize your tests - separate and categorize them
      - Create folders to more accurately categorize files
      - Split tests by category, e.g. in the 'user' folder put tests for login and registration, and in the 'shopping' folder - tests for the buying process.
- Write independent tests
      - You should be able to choose any test or run several tests in any order and still get the same result.
- Set base URL
      - Instead of repeating the lines with web addresses in each test, set the base URL in the `cypress.json` file, or if using Selenium, method `setDriver` within `src\main\java\package_directory\BaseClass`.
- Run tests, that are crucial at the moment
      - if Cypres user: use `cy.only` and `cy.skip`,
      - if Python Selenium user: Need to "Debug" script to the desired point that determined previously
- Use your own custom commands, and page objects
      - Add your command to the `commands.js` file
- Use the best selectors like **CSS**, **By ID**, **By ClassName**, **xPath** (In Selenium and Python Only)
- Add assertions - to be sure that you are referring to the right element at the right time instead of "static" wait(ms).
      - Using Cypress: `should('contain.text', 'Login')`
      - Using Cypress: `should('be.visible')`
      - Using Selenium, Python: `self.applicant_acc_settings.profile_picture.is_present()`
- Don't forget to stretch in between the tests

## 03. Coding Standards

- **Quotes:** Use single-quotes for string literals, e.g. **'my-identifier'**, but use double-quotes for strings that are likely to contain single-quote characters as part of the string itself (such as error messages, or any strings containing natural language), e.g. **"You've got an error!"**. For triple-quoted strings, always use double quote characters to be consistent with the docstring convention in PEP 257.
- **Imports grouping:** Should be grouped in the following order (Python Selenium Only)
      * standard library imports
      * related third party imports
      * local application/library specific imports
- **Import code:** You should put a blank line between each group of imports. **Don’t use joker** `from module import *`. Instead list the names you need explicitly: `from module import name1, name2`. Examples:
      - Cypress:
          - `import { onNavigations } from "../../../support.page_obejcts/navigations"`
      - Selenium, Python:
          - `from project.page_objects.registration.welcome_page import WelcomePage`
- **Consistent Indentation:** (Python Only)
      - Spaces are the preferred indentation method. Tabs should be used solely to remain consistent with code that is already indented with tabs. Python 3 disallows mixing the use of tabs and spaces for indentation. Python 2 code indented with a mixture of tabs and spaces should be converted to using spaces exclusively.
      - When invoking the Python 2 command line interpreter with the -t option, it issues warnings about code that illegally mixes tabs and spaces. When using -tt these warnings become errors. **These options are highly recommended!**
- **Avoid Obvious Comments:**
      - Commenting your code is fantastic; however, it can be overdone or just be plain redundant. Take this example:

            # get the country code
            country_code = get_country_code(remote_address)
            # if country code is US
            if  country_code == 'US':
            // display the form input for state
            form_input_state()

      - When the text is that obvious, it's really not productive to repeat it within comments. If you must comment on that code, you can simply combine it to a single line instead:

            # display state selection for US users
            country_code = get_country_code(remote_address)
            If country_code == 'US':
            form_input_state()

- **Code Grouping:**
      - More often than not, certain tasks require a few lines of code. It is a good idea to keep these tasks within separate blocks of code, with some spaces between them:

            # import local application/library
            from xpath import xpath_list

## 04. Framework Guidelines

- Test case data should be kept in separate files.
- Logs and Reports folder should be maintained.
- Proper logs should be maintained.
- Html Reports should be generated.
- Rerunning of automation scripts should not override the old reports.
- Proper Exception Handling should be done.
- Your framework should contain the Readme.md file which explains the execution steps.
- Setup file should be created which includes all dependencies installation.
- Virtual environment of python should be done correctly so that all dependencies and packages installed separately.
- Make sure the objective for your code is clear. That reflects, use meaningful names for your variables, functions, classes, modules, files etc. While doing so, make sure you don't accidentally use the standard function name or module name. (Refer:`http://pycogent.org/coding_guidelines.html`).
- Add proper module, class, function level doc-comments. If possible, include example in doc-comments.
- Don’t try to put too much code under one function. 200 lines is an absolute maximum – 50 is usually more reasonable.
- Remove the commented code. If required, it could be brought back by source revisions.
- Remove any duplicate code if exist. This could be done by moving duplicated code under a function and calling those instead.
- Check if code could be simpler. Break complex routines down into smaller ones, consider using built-in functions and data types instead of custom ones, see if complex logic can be separated into a dispatch routine and simple individual functions, etc.
- Make use of existing framework functions/APIs/utility methods instead of writing new.
- If any improvement on existing framework is possible, do that but get it reviewed prior to commit.
- Follow the PEP8 guidelines. It's much more easy from the IDE you use. Like for ex: In PyCharm you can convert your code as per PEP8 standards by shortcut Opt+Cmd+L.

## 05. About HTML Elements

- Html DOM consists of: HTML Tags, HTML Attributes and Attributes values
- Class and ID are also HTML attribute names
- Class attribute can have several values and each value is separated by space
- HTML tags usually come in pairs of Opening and Closing tag. Closing tag has the same name and forward slash
- Value in between angle brackets (>here<) is a plain text
- Elements above the “key” web element are Parent elemens
- Elements inside of the “key” Element are Child Elements
- Elements placed at the same level side by side are Sibiling Elements
- Remember to use the best locators (never use absolute XPath or absolute CSS).
- ID and data attributes are the best for locating elements.

## 06. Bad Coding - Good Coding

### Examples

#### Wait for page to load

<details>
<summary>BAD</summary>

```java
public void waitForPageToLoad(String string){
  String url = browserDriver.getCurrentUrl();
  Boolean stringExists = url.contains(string);
  int Count = 0;

  while (Boolean.FALSE.equals(stringExists) && Count < 100){
     url = browserDriver.getCurrentUrl();
     stringExists = url.contains(string);
     Count = Count + 1;
     try {
        Thread.sleep(500);
     } catch (InterruptedException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
     }
  }

   //if(stringExists==false)
   if(Boolean.FALSE.equals(stringExists))
      throw new IllegalStateException(
        “Page not loaded. Current page” + “is: “
         + browserDriver.getCurrentUrl());
   }
}
```

</details>

<details>
<summary>GOOD</summary>

```java
public void isUrlCorrect(String keyword){ 
  int count = 0;

   do {
     count ++;
     waitFor(1);
   } while (urlContains(keyword) == false && count < 30);

   if(count == 30)
     throw new PageNotDisplayedException(“Page not loaded”);; 
}
private void urlContains(String keyword) {
   return driver.getCurrentUrl().contains(keyword);
}
```

</details>
<details>
<summary>BEST</summary>

```java
WebDriverWait wait = new WebDriverWait(driver, 30);
wait.until(ExpectedConditions.urlContains(keyword));
```

</details>

#### Click elements

<details>
<summary>BAD</summary>

```java
public void Click(){
  
  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
  try {
    wait.until(ExpectedConditions.elementToBeClickable(locator));
    driver.findElement(locator).click();
    System.out.println(“Clicked webelement at: “+locator );
  }
  catch (RuntimeException e) {
    System.out.println(e);
    WaitMilliSeconds(500);
    if (Boolean.FALSE.equals(
                    driver.findElement(locator).isEnabled()))
       throw new NoSuchElementException(
          “Elment not found at locator: “+ locator);
    driver.findElement(locator).click();
 }

}
```

</details>
<details>
<summary>GOOD</summary>

```java
public void click(){
 WebElement element = this.wait.until(ExpectedConditions.
                            elementToBeClickable(this.locator));
 element.click();
}
```

</details>

#### Send keys

<details>
<summary>BAD</summary>

```java
public void SendKeys(String text){
 wait = (new WebDriverWait(driver, 4));
 try
 {
    wait.until(ExpectedConditions
        .visibilityOfElementLocated(locator));
    driver.findElement(locator).sendKeys(text);
    System.out.println(“Entered sendKey text: “+text);
    //SendKeyEnter();
    System.out.println(“Click Enter”);
    WaitMilliSeconds(1000);
 }
 catch(RuntimeException e)
 {
    System.out.println(“\nSecond attempt to SendKey text:” + text);
    Wait();
    driver.findElement(locator).sendKeys(text);
    SendKeyEnter();
 }

}
```

</details>
<details>
<summary>GOOD</summary>

```java
public void sendKeys(String text){
  WebElement element = wait.until(ExpectedConditions
                          .visibilityOfElementLocated(locator));
  element.sendKeys(text);
}
```

</details>

#### Wait until web element is clickable

<details>
<summary>BAD</summary>

```java
public void WaitUntilWebElementClickable(){
 try {
   wait.until(ExpectedConditions.elementToBeClickable(locator));
 }
 catch (RuntimeException e) {
   WaitMilliSeconds(2000);
   driver.navigate().refresh();
   System.out.println(“refresh internally…”);
   wait.until(ExpectedConditions.elementToBeClickable(
        driver.findElement(locator)));
 }
}
```

</details>
<details>
<summary>GOOD</summary>

```java
public void waitUntilElementClickable(){
 wait.until(ExpectedConditions.elementToBeClickable(locator));
}
```

</details>

#### Get text

<details>
<summary>BAD</summary>

```java
public String getText() {
 try {
   wait.until(ExpectedConditions
          .visibilityOfElementLocated(locator));
   return driver.findElement(locator).getText().toString().trim();
 }
 catch (RuntimeException e){
   System.out.println(“getText() exception: “+ e);
   return “”;
 }
}
```

</details>
<details>
<summary>GOOD</summary>

```java
public String getText() {
 WebElement element = wait.until(ExpectedConditions.
                         visibilityOfElementLocated(locator));
 return element.getText().trim();
}
```

</details>

## 07. Useful Links

- [Docstring Conventions](https://www.python.org/dev/peps/pep-0257/)
- [Cypress Best Practice](https://docs.cypress.io/guides/references/best-practices)
- [Selenium Best Practice](https://www.lambdatest.com/blog/selenium-best-practices-for-web-testing/)
- [Selenium Best Practice - TestProjectIO](https://blog.testproject.io/2020/12/03/selenium-automation-best-practices-tips-you-must-know/)
- [Software Testing Guidelines](https://sceweb.sce.uhcl.edu/helm/WEBPAGE-SWtesting/my_files/Quick%20Guide/software_testing__quick_guide.html)
