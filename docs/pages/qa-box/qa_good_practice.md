# [`QA Box`] Good Automation Practice

There are a lot of reasons test automation is beneficial, and by adhering to automated testing best practices you can ensure that your testing strateagy delivers the maximum return on investment (ROI). Automated testing will shorten your development cycles, avoid cumbersome repetitive tasks and help improve software quality but how do you get started? These best practices a successful foundation to start improving your software quality.

Thorough testing is crucial to the success of a software product. If your software doesn’t work properly, chances are strong that most people won’t buy or use it…at least not for long. But testing to find defects – or bugs – is time-consuming, expensive, often repetitive, and subject to human error. Automated testing, in which Quality Assurance teams use software tools to run detailed, repetitive, and data-intensive tests automatically, helps teams improve software quality and make the most of their always-limited testing resources. Test automation tools help teams test faster, allows them to test substantially more code, improves test accuracy, and frees up QA engineers so they can focus on tests that require manual attention and their unique human skills.

Use these top tips to ensure that your software testing is successful and you get the maximum return on investment (ROI):

## Decide what Test Cases to Automate

- Repetitive tests that run for multiple builds
- Tests that tend to cause human error
- Tests that require multiple data sets.
- Frequently used functionality that introduces high risk conditions.
- Tests that are impossible to perform manually.
- Tests that run on several different hardware or software platforms and configurations.
- Tests that take a lot of effort and time when manual testing.

### Success in test automation recipe

- Careful plan and design work
- Creating an automation plan (initial set of tests to automate)
- Define your goal for automated testing
- Determine which types of tests to automate:
      - Each types of testing has its place in the testing process.
- Decide what actions your automated tests will perform.
- Divide your tests into several logical, smaller tests
- Test the functionality of your application as you add it, rather than waiting until the whole feature is implemented.
- When creating tests, try to keep them small and focused on one objective

Once you create several simple automated tests, you can group your tests into one, larger automated test. You can organize automated tests by the application’s functional area, major/minor division in the application, common functions or a base set of test data. If an automated test refers to other tests, you may need to create a test tree, where you can run tests in a specific order.

## Select the Right Automated Testing Tool

There are a lot of automated testing tools on the market, and it is important to choose the automated testing tool that best suits your overall requirements.

Consider these key points when selecting an automated testing tool:

- Support for your platforms and technology. Are you testing .Net, C# or WPF applications and on what operating systems? Are you going to test web applications? Do you need support for mobile application testing? Do you work with Android or iOS, or do you work with both operating systems?
- Flexibility for testers of all skill levels.
- Feature rich but also easy to create automated tests. Does the automated testing tool support record-and-playback test creation as well as manual creation of automated tests; does it include features for implementing checkpoints to verify values, databases, or key functionality of your application?
- Create automated tests that are reusable, maintainable and resistant to changes in the applications UI. Will my automated tests break if my UI changes?
- Integrate with your existing ecosystem. Does your tool integrate with your CI/CD pipeline such as Jenkins or Azure DevOps? Or your test management framework such as Zephyr? What about a defect-management system like Jira, or a source control such as Git?

## Divide your Automated Testing Efforts

It is important to identify the level of experience and skills for each of your team members and divide your automated testing efforts accordingly.

In order to perform writing automated test scripts, you should have QA engineers that know the script language provided by the automated testing tool.

Some team members may not be versed in writing automated test scripts. These QA engineers may be better at writing test cases. Consider using keyword-driven testing. Keyword tests are often seen as an alternative to automated test scripts. Unlike scripts, they can be easily used by technical and non-technical users and allow users of all levels to create robust and powerful automated tests.

You should also collaborate on your automated testing project with other QA engineers in your department. Testing performed by a team is more effective for finding defects and the right automated testing tool allows you to share your projects with several testers.

## Create Good, Quality Test Data

The data that should be entered into input fields during an automated test is usually stored in an external file.

This data might be read from a database or any other data source like text or XML files, Excel sheets, and database tables. A good automated testing tool actually understands the contents of the data files and iterates over the contents in the automated test. Using external data makes your automated tests reusable and easier to maintain. To add different testing scenarios, the data files can be easily extended with new data without needing to edit the actual automated test.

Invest time and effort into creating data that is well structured. The earlier you create good-quality data, the easier it is to extend existing automated tests along with the application's development.

## Create Automated Tests that are Resistant to Changes in the UI

The user interface of the application may change between builds, especially in the early stages. These changes may affect the test results, or your automated tests may no longer work with future versions of the application.

Provide unique names for your controls, it makes your automated tests resistant to these UI changes and ensures that your automated tests work without having to make changes to the test itself.

## Keep Test Records In a Bug Database

Using a bug database is a best practice whether a team uses test automation or not.

Whenever new bugs are found by the automation tool or by the testers, they should be recorded in a bug tracking tool with the exact steps to reproduce the bugs and other details.

## Conclusion

Adapting these recommended best practices can help you avoid common mistakes and improve your automated testing process. This helps you test faster, save money and get your products released on time.
