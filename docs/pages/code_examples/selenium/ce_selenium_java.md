# Selenium-Java Example

## Initialize

```java
Maven: selenium-firefox-driver
import org.openqa.selenium.firefox.FirefoxDriver;
WebDriver driver = new FirefoxDriver();

// Maven: selenium-safari-driver
import org.openqa.selenium.safari.SafariDriver;
WebDriver driver = new SafariDriver();

// Maven: selenium-chrome-driver
import org.openqa.selenium.chrome.ChromeDriver;
WebDriver driver = new ChromeDriver();
```

## Locators

```java
driver.findElement(By.className("className"));
driver.findElement(By.cssSelector("css"));
driver.findElement(By.id("id"));
driver.findElement(By.linkText("text"));
driver.findElement(By.name("name"));
driver.findElement(By.partialLinkText("pText"));
driver.findElement(By.tagName("input"));
driver.findElement(By.xpath("//*[@id='editor']"));
// Find multiple elementsList
<WebElement> anchors = driver.findElements(By.tagName("a"));
// Search for an element inside anotherWebElement 
div = driver.findElement(By.tagName("div")).findElement(By.tagName("a"));
```

## Basic Elements Operations
```java
WebElement element = driver.findElement(By.id("id"));
element.click();
element.sendKeys("someText");
element.clear();
element.submit();
String innerText = element.getText();
boolean isEnabled = element.isEnabled();
boolean isDisplayed = element.isDisplayed();
boolean isSelected = element.isSelected();
WebElement element = driver.findElement(By.id("id"));
Select select = new Select(element);
select.selectByIndex(1);
select.selectByVisibleText("Ford");
select.selectByValue("ford");
select.deselectAll();
select.deselectByIndex(1);
select.deselectByVisibleText("Ford");
select.deselectByValue("ford");
List<WebElement> allSelected = select.getAllSelectedOptions();
boolean isMultipleSelect = select.isMultiple();
```

## Advanced Element Operations

```java
// Drag and Drop
WebElement element = driver.FindElement(By.xpath("//[@id='project']/p[1]/div/div[2]"));
Actions move = new Actions(driver);
move.dragAndDropBy(element, 30, 0).build().perform();
// How to check if an element is visible
Assert.assertTrue(driver.findElement(By.xpath("//[@id='tve_editor']/div")).isDisplayed());
// Upload a file
WebElement element = driver.findElement(By.id("RadUpload1file0"));
String filePath = "D:WebDriver.Series.TestsWebDriver.xml";
element.sendKeys(filePath);
// Scroll focus to control
WebElement link =driver.findElement(By.partialLinkText("Previous post"));
String js = String.format("window.scroll(0, %d);",link.getLocation().getY());
((JavascriptExecutor)driver).executeScript(js);link.click();
// Taking an element screenshot
WebElement element =driver.findElement(By.xpath("//[@id='tve_editor']/div"));
File screenshotFile =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
BufferedImage fullImg = ImageIO.read(screenshotFile);
Point point = element.getLocation();
int elementWidth = element.getSize().getWidth();
int elementHeight = element.getSize().getHeight();
BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), elementWidth, elementHeight);
ImageIO.write(eleScreenshot, "png", screenshotFile);
String tempDir = getProperty("java.io.tmpdir");
File destFile = new File(Paths.get(tempDir, fileName + ".png").toString());
FileUtils.getFileUtils().copyFile(screenshotFile, destFile);
// Focus on a control
WebElement link =driver.findElement(By.partialLinkText("Previous post"));
Actions action = new Actions(driver);
action.moveToElement(link).build().perform();
// Wait for visibility of an element
WebDriverWait wait = new WebDriverWait(driver, 30);
wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//[@id='tve_editor']/div[2]/div[2]/div/div")));
```

## Basic Browser Operations

```java
// Navigate to a page
driver.navigate().to("http://google.com");
// Get the title of the page
String title = driver.getTitle();
// Get the current URL
String url = driver.getCurrentUrl();
// Get the current page HTML source
String html = driver.getPageSource();
```

## Advanced Browser Operations

```java
// Handle JavaScript pop-ups
Alert alert = driver.switchTo().alert();
alert.accept();
alert.dismiss();
// Switch between browser windows or tabs
Set<String> windowHandles = driver.getWindowHandles();
String firstTab = (String)windowHandles.toArray()[1];
String lastTab = (String)windowHandles.toArray()[2];
driver.switchTo().window(lastTab);
// Navigation history
driver.navigate().back();
driver.navigate().refresh();
driver.navigate().forward();
// Maximize windowdriver.manage().window().maximize();
// Add a new cookieCookie newCookie = new Cookie("customName", "customValue");
driver.manage().addCookie(newCookie);
// Get all cookies
Set<Cookie> cookies = driver.manage().getCookies();
// Delete a cookie by name
driver.manage().deleteCookieNamed("CookieName");
// Delete all cookies
driver.manage().deleteAllCookies();
//Taking a full-screen screenshot
File srceenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
String tempDir = getProperty("java.io.tmpdir");
File destFile = new File(Paths.get(tempDir, fileName + ".png").toString());
FileUtils.getFileUtils().copyFile(srceenshotFile, destFile);
// Wait until a page is fully loaded via JavaScript
WebDriverWait wait = new WebDriverWait(driver, 30);
wait.until(x -> {((String)((JavascriptExecutor)driver).executeScript("return document.readyState")).equals("complete");});
// Switch to frames
driver.switchTo().frame(1);driver.switchTo().frame("frameName");
WebElement element = driver.findElement(By.id("id"));
driver.switchTo().frame(element);
// Switch to the default document
driver.switchTo().defaultContent();
```

## Advanced Browser Configurations

```java
// Set a HTTP proxy Chromevar proxy = new Proxy();
proxy.setProxyType(Proxy.ProxyType.MANUAL);
proxy.setAutodetect(false);
proxy.setSslProxy("127.0.0.1:3239");
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.setProxy(proxy);
driver = new ChromeDriver(chromeOptions);
// Accept all certificates 
ChromeChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.addArguments("--ignore-certificate-errors");
driver = new ChromeDriver(chromeOptions);
// Set Chrome options
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.addArguments("user-data-dir=C:PathToUser Data");
driver = new ChromeDriver(chromeOptions);
// Set the default page load timeout
driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
// Start Chrome with an unpacked extension
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.addArguments("load-extension=/path/to/extension");
driver = new ChromeDriver(chromeOptions);
// Start Chrome with a packed extension
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.addExtensions(new File("local/path/to/extension.crx"));
driver = new ChromeDriver(chromeOptions);
```

## Taking a Screenshot

You can use the following method to take a full-screen screenshot of the browser.

```java
public void takeFullScreenshot(String fileName) throws Exception {
 File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
 String tempDir = getProperty("java.io.tmpdir");
 File destFile = new File(Paths.get(tempDir, fileName + ".png").toString());
 FileUtils.getFileUtils().copyFile(srcFile, destFile);
}
```

Sometimes you may need to take a screenshot of a single element.

```java
public void takeScreenshotOfElement(WebElement element, String fileName) throws Exception {
 File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
 BufferedImage fullImg = ImageIO.read(screenshotFile);
 Point point = element.getLocation();
 int elementWidth = element.getSize().getWidth();
 int elementHeight = element.getSize().getHeight();
 BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), elementWidth, elementHeight);
 ImageIO.write(eleScreenshot, "png", screenshotFile);
 String tempDir = getProperty("java.io.tmpdir");
 File destFile = new File(Paths.get(tempDir, fileName + ".png").toString());
 FileUtils.getFileUtils().copyFile(screenshotFile, destFile);
}
```

First, we make a full-screen screenshot then we locate the specified element by its location and size attributes. After that, the found rectangle chunk is saved as a BufferedImage.

Here is how you use both methods in tests:

```java
@Test
public void takeFullScreenshot_test() throws Exception {
 driver.navigate().to("http://automatetheplanet.com");
 takeFullScreenshot("testImage");
}
@Test
public void takeElementScreenshot_test() throws Exception {
 driver.navigate().to("http://automatetheplanet.com");
 var element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/header/div/div[2]/div/div[2]/nav")));
 takeScreenshotOfElement(element, "testElementImage");
}
```

## Get HTML Source of WebElement

You can use the getAttribute method of the WebElement interface to get the inner HTML of a specific element.

```java
@Test
public void getHtmlSourceOfWebElement() {
 driver.navigate().to("http://automatetheplanet.com");
 var element = wait.until(ExpectedConditions.visibilityOfElementLocated(
 By.xpath("/html/body/div[1]/header/div/div[2]/div/div[2]/nav")));
 String sourceHtml = element.getAttribute("innerHTML");
 System.out.println(sourceHtml);
}
```

## Execute JavaScript

You can use the interface JavaScriptExecutor to execute JavaScript through WebDriver.

```java
@Test
public void executeJavaScript() {
 driver.navigate().to("http://automatetheplanet.com");
 JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
 String title = (String) javascriptExecutor.executeScript("return document.title");
 System.out.println(title);
}
```

This test will take the current window's title via JavaScript and print it to the output window.

## Set Page Load Timeoout

There are at least three methods that you can use for the job.

First, you can set the default driver's page load timeout through the WebDriver's options class.

```java
driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
```

You can wait until the page is completely loaded via JavaScript.

```java
private void waitUntilLoaded() {
 wait.until(x ->
 {
 JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
 String isReady = (String) javascriptExecutor.executeScript("return document.readyState");
 return isReady.equals("complete");
 });
}
```

Your third option is to wait for a specific element(s) to be visible on the page.

```java
wait.until(ExpectedConditions.visibilityOfAllElements(
 driver.findElements(By.xpath("//*[@id='tve_editor']/div[2]/div[2]/div/div"))));
```

## Execute Tests in a Headless Browser

You can use the ChromeOptions class and set a few arguments to set up Chrome to run without its UI in the so-called headless mode. You can do the same for Firefox and Edge Chromium.

```java
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.addArguments(
"--headless", 
"--disable-gpu", 
"--window-size=1920,1200",
"--ignore-certificate-errors");
driver = new ChromeDriver(chromeOptions);
```

## Check If an Element Is Visible

You can use the isDisplayed method of the WebElement interface.

```java
@Test
public void checkIfElementIsVisible() {
 driver.navigate().to("http://automatetheplanet.com");
 var element = driver.findElement(By.xpath("/html/body/div[1]/header/div/div[2]/div/div[2]/nav");
 Assert.assertTrue(element).isDisplayed());
}
```

## Manage Cookies

Before you can work with the cookies of a site, you need to navigate to some of its pages.

```java
@Test
public void manageCookies() {
 driver.navigate().to("http://automatetheplanet.com");
 // get all cookies
 var cookies = driver.manage().getCookies();
 for (Cookie cookie:cookies) {
 System.out.println(cookie.getName());
 }
 // get a cookie by name
 var fbPixelCookie = driver.manage().getCookieNamed("_fbp");
 // create a new cookie by name
 Cookie newCookie = new Cookie("customName", "customValue");
 driver.manage().addCookie(newCookie);
 // delete a cookie
 driver.manage().deleteCookie(fbPixelCookie);
 // delete a cookie by name
 driver.manage().deleteCookieNamed("customName");
 // delete all cookies
 driver.manage().deleteAllCookies();
}
```

## Maximize Window

Use the maximize method of the Window interface.

```java
driver.manage().window().maximize();
```

## Drag and Drop

You can use the special Actions WebDriver class to perform complex UI interactions. Through its method dragAndDropBy, you can drag and drop. You only need to set the desired X and Y offsets.

```java
@Test
public void dragAndDrop() {
 driver.navigate().to("http://loopj.com/jquery-simple-slider/");
 var element = driver.findElement(By.xpath("//*[@id='project']/p[1]/div/div[2]"));
 Actions action = new Actions(driver);
 action.dragAndDropBy(element, 30, 0).build().perform();
}
```

## Upload a File

It is a straightforward task to upload a file using WebDriver. You need to locate the file element and use the WebElement's sendKeys method to set the path to your file.

```java
@Test
public void fileUpload() throws IOException {
 driver.navigate().to(
 "https://demos.telerik.com/aspnet-ajax/ajaxpanel/application-scenarios/file-upload/defaultcs.aspx");
 var element = driver.findElement(By.id("ctl00_ContentPlaceholder1_RadUpload1file0"));
 String filePath = Paths.get(getProperty("java.io.tmpdir"), "debugWebDriver.xml").toString();
 File destFile = new File(filePath);
 destFile.createNewFile();
 element.sendKeys(filePath);
}
```

## Handle JavaScript Pop-ups

You can use the accept and dismiss methods of the Alert interface to handle JavaScript pop-ups. First, you need to use the switchTo method to switch to the alert.

```java
@Test
public void handleJavaScripPopUps() {
 driver.navigate().to("http://www.w3schools.com/js/tryit.asp?filename=tryjs_confirm");
 driver.switchTo().frame("iframeResult");
 var button = driver.findElement(By.xpath("/html/body/button"));
 button.click();
 Alert alert = driver.switchTo().alert();
 if (alert.getText().equals("Press a button!")) {
 alert.accept();
 }
 else {
 alert.dismiss();
 }
}
```

## Switch Between Browser Windows or Tabs

WebDriver drives the browser within a scope of one browser window. However, we can use its switchTo method to change the target window or tab.

```java
@Test
public void movingBetweenTabs() {
 driver.navigate().to("https://www.automatetheplanet.com/");
 var firstLink = driver.findElement(By.xpath("//*[@id='menu-item-11362']/a"));
 var secondLink = driver.findElement(By.xpath("//*[@id='menu-item-6']/a"));
 String selectLinkOpenninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
 firstLink.sendKeys(selectLinkOpenninNewTab);
 secondLink.sendKeys(selectLinkOpenninNewTab);
 Set<String> windows = driver.getWindowHandles();
 String firstTab = (String)windows.toArray()[1];
 String lastTab = (String)windows.toArray()[2];
 driver.switchTo().window(lastTab);
 Assert.assertEquals("Resources - Automate The Planet", driver.getTitle());
 driver.switchTo().window(firstTab);
 Assert.assertEquals("Blog - Automate The Planet", driver.getTitle());
}
```

The getWindowHandles method returns all open browser windows. You can pass the name of the desired tab/window to the window method of the TargetLocator interface (returned by the switchTo method) to change the current target.

You can also notice that we use one more trick to open the link in a new tab using the Keys.chord method, and then instead of clicking, we use the sendKeys method to perform CTRL + return sequence.

## Navigation History

WebDriver Navigation interface contains handy methods for going forward and backward. Also, you can refresh the current page.

```java
@Test
public void navigationHistory() {
 driver.navigate().to("https://www.codeproject.com/Articles/1078541/Advanced-WebDriver-Tips-and-Tricks-Part");
 driver.navigate().to("http://www.codeproject.com/Articles/1017816/Speed-up-Selenium-Tests-through-RAM-Facts-and-Myth");
 driver.navigate().back();
 Assert.assertEquals("10 Advanced WebDriver Tips and Tricks - Part 1 - CodeProject", driver.getTitle());
 driver.navigate().refresh();
 Assert.assertEquals("10 Advanced WebDriver Tips and Tricks - Part 1 - CodeProject", driver.getTitle());
 driver.navigate().forward();
 Assert.assertEquals("Speed up Selenium Tests through RAM Facts and Myths - CodeProject", driver.getTitle());
}
```

## Handle SSL Certificate Error ChromeDriver

```java
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.addArguments("--ignore-certificate-errors");
driver = new ChromeDriver(chromeOptions);
```

## Scroll Focus to Control

There isn't a built-in mechanism in WebDriver to scroll focus to a control. However, you can use the JavaScript's method window.scroll. You only need to pass the getY location of the desired element.

```java
@Test
public void scrollFocusToControl() {
 driver.navigate().to("http://automatetheplanet.com/");
 var ourMissionLink = driver.findElement(By.xpath("//*[@id="panel-6435-0-0-4"]/div"));
 String jsToBeExecuted = String.format("window.scroll(0, {0});", ourMissionLink.getLocation().getY());
 JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
 javascriptExecutor.executeScript(jsToBeExecuted);
}
```

## Focus on a Control

To do the trick, we use again the special Actions class, which has a method called moveToElement.

```java
@Test
public void focusOnControl() {
 driver.navigate().to("http://automatetheplanet.com/");
 waitUntilLoaded();
 var ourMissionLink = driver.findElement(By.xpath("//*[@id="panel-6435-0-0-4"]/div"));
 Actions action = new Actions(driver);
 action.moveToElement(ourMissionLink).build().perform();
}
```

## Set HTTP Proxy ChromeDriver

The configuration of a proxy for ChromeDriver is a little bit different from the one for FirefoxDriver. You need to use the special Proxy class in combination with ChromeOptions.

```java
var proxy = new Proxy();
proxy.setProxyType(Proxy.ProxyType.MANUAL);
proxy.setAutodetect(false);
proxy.setSslProxy("127.0.0.1:3239");
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.setProxy(proxy);
driver = new ChromeDriver(chromeOptions);
```

## Set HTTP Proxy with Authentication ChromeDriver

The only difference from the previous example is the configuration of the --proxy-server argument.

```java
var proxy = new Proxy();
proxy.setProxyType(Proxy.ProxyType.MANUAL);
proxy.setAutodetect(false);
proxy.setSslProxy("127.0.0.1:3239");
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.setProxy(proxy);
chromeOptions.addArguments("--proxy-server=http://user:password@127.0.0.1:3239");
driver = new ChromeDriver(chromeOptions);
```

## Start ChromeDriver with an Unpacked Extension

Chrome extensions can be either packed or unpacked. Packed extensions are a single file with a .crx extension. Unpacked Extensions are a directory containing the extension, including a manifest.json file. To load an unpacked extension you need to set the load-extension argument.

```java
chromeOptions.addArguments("load-extension=/pathTo/extension");
driver = new ChromeDriver(chromeOptions);
```

## Start ChromeDriver with an Packed Extension

Instead of setting the load-extension argument, you need to use the addExtension method of the ChromeOptions class to set the path to your packed extension.

```java
ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.addExtensions(new File("local/path/to/extension.crx"));
driver = new ChromeDriver(chromeOptions);
```

## Assert a Button Enabled or Disabled

You can check if an element is disabled through the isEnabled method of the WebElement interface.

```java
@Test
public void assertButtonEnabledDisabled() {
 driver.navigate().to("http://www.w3schools.com/tags/tryit.asp?filename=tryhtml_button_disabled");
 driver.switchTo().frame("iframeResult");
 var button = driver.findElement(By.xpath("/html/body/button"));
 Assert.assertFalse(button.isEnabled());
}
```

## Verify File Downloaded ChromeDriver

To change the default download directory of the current Chrome instance set the download.default_directory argument. When we initiate the file download, we use the WebDriverWait to wait until the file exists on the file system. Finally, we assert the file size. The whole code is surrounded with a try-finally block. In the finally we delete the downloaded file so that our test to be consistent every time.

```java
String downloadFilepath = "c:temp";
HashMap<String, Object> chromePrefs = new HashMap<>();
chromePrefs.put("profile.default_content_settings.popups", 0);
chromePrefs.put("download.default_directory", downloadFilepath);
chromeOptions.setExperimentalOption("prefs", chromePrefs);
chromeOptions.addArguments("--test-type");
chromeOptions.addArguments("start-maximized", "disable-popup-blocking");

String downloadFilepath = "c:temp";
HashMap<String, Object> chromePrefs = new HashMap<>();
chromePrefs.put("profile.default_content_settings.popups", 0);
chromePrefs.put("download.default_directory", downloadFilepath);
chromeOptions.setExperimentalOption("prefs", chromePrefs);
chromeOptions.addArguments("--test-type");
chromeOptions.addArguments("start-maximized", "disable-popup-blocking");

@Test
public void VerifyFileDownloadChrome() throws IOException {
 var expectedFilePath = Paths.get("c:tempTesting_Framework_2015_3_1314_2_Free.exe");
 try
 {
 driver.navigate().to("https://www.telerik.com/download-trial-file/v2/telerik-testing-framework");
 wait.until(x -> Files.exists(expectedFilePath));
 long bytes = Files.size(expectedFilePath);
 Assert.assertEquals(4326192, bytes);
 }
 finally
 {
 if (Files.exists(expectedFilePath))
 {
 Files.delete(expectedFilePath);
 }
 }
}
```