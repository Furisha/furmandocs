# Seleniumbase-Python Examples

## HomePage (Page Object Model)

- Create POM "page_objects" folder
- Within POM "page_objects" folder create "home_page.py" file which contains element locators, login actions, and open_page url

- Step 1. Import setup from SeleniumBase (BaseCase class inherits from the unittest.TestCase class)

```python
from seleniumbase import BaseCase
```

- Step 2. Create class HomePage(BaseCase)

```python
class HomePage(BaseCase):
```

- Step 3. Find web elements and store it in attribute value.

```python
    # home page elements
    logo_icon = ".custom-logo-link"
    get_started_btn = "#get-started"
    heading_text = "h1"
    copyright_text = ".tg-site-footer-section-1"
    menu_links = "//*[starts-with(@id, 'menu-item')]"
    username = "#username"
    password = "#password"
    login_button = "button[name=login]"
    logout_button = ".woocommerce-MyAccount-content a[href*=logout]"
    title = "Practice E-Commerce Site – Automation Bro"
```

- Step 4. Create page object that we can use later in out HomeTest(HomePage) and call it "login"

```python
    def login(self):
```

- Step 5. Within login create this actions for:
    1. Open homepage url (**_self.open()_**)
    2. Type text in username filed (**_self.add_text_()**)
    3. Type text in password filed
    4. Click on login button (**_self.click()_**)
    5. Assert text for Log out button (**_self.assert_text()_**)

```python
        # actions for successful login and assertion
        self.open("https://practice.automationbro.com/my-account")
        self.add_text(self.username, "testuser00")
        self.add_text(self.password, "zcbAKa4j7LpXiC6")
        self.click(self.login_button)
        self.assert_text("Log out", ".woocommerce-MyAccount-content")
```

- Step 6. Create one more page object (function, method...) and name it "open_page"

```python
        # action: open page
        self.open("https://practice.automationbro.com")
```

## HomeTest (Test)

- Create "tests" folder
- Within "tests" folder create "test_home.py" file which represents our tests for home page test. It contains "setUp", "tearDown", "test_home_page", and additional "test_menu_links" test.

- Step 1. Import HomePage from POM "home_page"

```python
from page_objects.home_page import HomePage
```

- Step 2. Create "class HomeTest(HomePage):"

```python
class HomeTest(HomePage):
```

- Step 3. Create "setUp", and "tearDown". This test will start before and after each test (etc. login, logout).

```python
    def setUp(self):
        # call the parent BaseCase class setup method
        super().setUp()
        print("RUNNING BEFORE EACH TEST")

        # login
        self.login()

        # open home page
        self.open_page()

    def tearDown(self):
        print("RUNNING AFTER EACH TEST")

        # logout
        self.open("https://practice.automationbro.com/my-account")
        self.click(HomePage.logout_button)
        self.assert_element_visible(HomePage.login_button)

        super().tearDown()
```

- Step 4. Create test_home_page test. This is actual our test for home page

```python
    def test_home_page(self):
```

- Step 5. Create actions for our test (included in requirements or acceptance criteria):

    1. Assert page title (**_self.assert_title()_**)
    2. Assert logo (**_self.assert_element()_**)
    3. click on the get started button and assert the url (**_self.click_**), (**_self.assert_equal()_**), (**_self.assert_true()_**)
    4. Get the text of the header and assert the value (**_self.assert_text()_**)
    5. Scroll to bottom and assert the copyright text (**_self.scroll_to_bottom()_**)

```python
        # assert page title
        self.assert_title(HomePage.title)
        # assert logo
        self.assert_element(HomePage.logo_icon)
        # click on the get started button and assert the url
        self.click(HomePage.get_started_btn)
        get_started_url = self.get_current_url()
        self.assert_equal(get_started_url, "https://practice.automationbro.com/#get-started")
        self.assert_true("get-started" in get_started_url)
        # get the text of the header and assert the value
        self.assert_text("Think different. Make different.", HomePage.heading_text)
        # excercise: scroll to bottom and assert the copyright text
        self.scroll_to_bottom()
        # print(self.get_text("tg-site-footer-section-1"))
        self.assert_text("Copyright © 2020 Automation Bro", HomePage.copyright_text)
```

- Step 6. Create second test "test_menu_links"

```python
def test_menu_links(self):
```

- Step 7. Create actions for our second test

```python
        expected_links = ['About', 'Shop', 'Blog', 'Contact', 'My account', 'Home', 'About', 'Blog', 'Contact', 'Support Form']

        # find menu links elements
        menu_links_el = self.find_elements(HomePage.menu_links)

        # loop through menu links
        for idx, link_el in enumerate(menu_links_el):
            print(idx, link_el.text)
```

## Run test with [commands](https://seleniumbase.io/help_docs/customizing_test_runs/)

Type the following command in terminal

```python
pytest -k "HomeTest"
pytest -k "HomeTest" --dashboard
pytest -k "HomeTest" --dashboard --rs --headless
pytest -k "HomeTest" --html=report.html
pytest -k "HomeTest" --firefox
pytest -k "HomeTest" --safari
pytest -n=4 / number of chrome, safari, firefox instance
pytest -k "HomeTest" --headless

```

### Pytest options for SeleniumBase

SeleniumBase's pytest plugin lets you customize test runs from the CLI (Command-Line Interface), which adds options for setting/enabling the browser type, Dashboard Mode, Demo Mode, Headless Mode, Mobile Mode, Multi-threading Mode, Recorder Mode, reuse-session mode, proxy config, user agent config, browser extensions, html-report mode, and more.

## Link

[AutomationBro](https://www.youtube.com/watch?v=D0-QGMacMxA&ab_channel=AutomationBro-DilpreetJohal)
[SeleniumBase](https://seleniumbase.io/)
