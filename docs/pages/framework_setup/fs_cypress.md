# Cypress

## QA Automaton Framework Setup

This document is created to enable new employees to get the basic, starting knowledge on how web automation testing works.

### Main Task

Ensure the quality of the products that your team puts in in the hands of customers by automatig web application end users processes.

### Cypress Limitations

- Safari browsers
- Two browsers at the same time
- Multi Tabs
- Iframes
- Mobile testing
- Java support (JavaScript only)

### Pre-Requisites

- Install [GIT](https://git-scm.com/) (Version Control System)
- Make sure you have access to [company git repository](https://git.nosolutions.rs/)
- Make sure you have ssh access to your account and key is already added for account.
- Install [VSCode](https://code.visualstudio.com/) (Integrated Development Environment)

![Main Logo](../../assets/logo-furman(50).png){ align=left }

## 01. Setup JDK

- Install [JDK 11](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html) for Windows
- Setup environment variables for Java JDK within your PC
      - Navigate to System Properties **(hit: WinKey, type: env, hit: ENTER):**
      - Navigate to Environment Variables: **`System Properties\Advanced\Environment Variables`**
          - Name: **`JAVA_HOME`**
          - Destination **`C:\Program Files\Java\jdk-11.0.12`**

![Main Logo](../../assets/logo-furman(50).png){ align=left }

## 02. Create Working Directory

- Open **Command Promt** or **Terminal**
- Navigate to **`Documents`** folder, type **`cd Documents`** (Or type: **cd Doc** + hit: **Tab**, hit: **Enter**)
- Within **`Documents`** folder type **`mkdir TestingProjects`** (Folder that hold all of your projects)
- Within **`TestingProjects`** folder type **`mkdir CypressProject`** (This is your Working directory)

### Initialize

- Open **Command Promt** or **Terminal**
- Navigate to **`Documents/TestingProjects/CypressProject`** folder
- Type **`npm init`** and follow instructions

![Main Logo](../../assets/logo-furman(50).png){ align=left }

## 03. Install Cypress From Scratch

- Open **Command Promt** or **Terminal**
- Navigate to **`Documents/TestingProjects/CypressProject`** folder
      - Type **`npm install cypress --save-dev`** (Latest version), or
      - Type **`npm install cypress@6.8.0 --save-dev`**, or
      - Type **`npm install cypress@6.9.1 --save-dev`**, or
      - Type **`npm install cypress@7.7.0 --save-dev`**

### Start Cypress Test Runner

- Open **Command Promt** or **Terminal**
- Navigate to **`Documents/TestingProjects/CypressProject`** folder
      - Type **`node_modules\.bin\cypress open`** (Run in background)
      - Type **`node_modules\.bin\cypress open —headed`** (Run in UI)

![Main Logo](../../assets/logo-furman(50).png){ align=left }

## 04. Clone Cypress Project

- Open **Command Promt** or **Terminal**
- Navigate to **`Documents/TestingProjects/CypressProject`** folder
- Clone application under test
      - **`git clone https://github.com/filiphric/cypress-tau-course.git`**
      - **`npm install`** (install all dependencies within node_modules folder)
      - **`npm start`**
      - Open Chrome browser and navigate to **`http://localhost:4200`**
- Clone application outside test
      - **`git clone https://git.nosolutions.rs/vladan.furman/dating.git`**
      - **`npm install`** (install all dependencies)
      - **`npm start`**

![Main Logo](../../assets/logo-furman(50).png){ align=left }

## 05. Brief Details About The Files And Folders

- **project_name/cypress**
      - **fixtures** (This folder contains CSV, HTML, or JSON files)
          - example.json
              - example.csv
      - **integrations** (This folder provides a place that writes out test cases **`login.spec.js`**)

            import { onNavigations } from "../../../support/page_objects/navigations";
            import { onHelpFunc } from "../../../support/page_objects/helpers";

            describe(`login_test`, () => {

                beforeEach('open application', () => {
                    cy.openStageUrl()
                });

                it(`valid_login_test`, function () {

                    onHelpFunc.selectLanguageENG()

                    cy.intercept('POST', '**/login').as('postLogin')

                    onNavigations.navigateToLogin()
                    cy.get('input[name=email]').clear().type(this.users.user_email, { delay: 50 })
                    cy.get('input[name=password]').type(this.users.user_password, { delay: 50 })
                    cy.get('button[type=submit]').should('contain.text', 'LOGIN').click()

                    cy.wait('@postLogin').its('response.statusCode')
                        .should('eq', 200)

                    onNavigations.navigateToProfileMenu()
                    onNavigations.navigateToLogout()

                })

            });


      - **plugins** (This folder contains the plugins or listeners)
      `cypress\plugin\index.js`
      - **reports** (This folder contains html reports runnable within your browser)
          - mocha
          - mochareports (This folder contain report.html file)
      - **support** (Writes customized commands or reusable methods that are available for usage in all of your spec/test files.)
        - **page_objects**
            - **helpers.js** (Functions or actions. E.g. delete all photos, select language, api - post actions, api - setup default state)

                    deleteAllPhotos() {

                        cy.get('nav').next().find('div').find('img').then(listing => {
                            const photoCounter = Cypress.$(listing).length
                            expect(listing).to.have.length(photoCounter)

                            for (let i = 0; i < photoCounter; i++) {
                                cy.intercept('DELETE', '**/photo/*').as('deletePhoto')
                                cy.get('nav').next().find('span').should('contain.text', 'Waiting for Approval').first()
                                    .next().find('span').last().as('deletebutton')
                                cy.get('@deletebutton').should('contain.text', 'Delete').click({ force: true }).wait(2000).then(el => {  
                                    cy.get('#modal-root').find('button').first().should('contain.text', 'Delete').click().wait(2000)
                                })
                                cy.wait('@deletePhoto').its('response.statusCode').should('eq', 200)
                                cy.waitUntil(() => cy.get('[role="alert"]').click())
                            }
                        })
                    }

            - **navigations.js** (Functions or actions. E.g. navigate to log in, navigate to profile menu, navigate to search tab)

                    navigateToPhotosTab() {
                        cy.intercept('GET', '**/photo*').as('getPhoto')
                        cy.get('nav').find('a[href="/profile/photos/public"]').click()
                        cy.wait('@getPhoto').its('response.statusCode')
                            .should('be.oneOf', [200])
                    }

            - **login_page.js** (Functions or actions. E.g. login different users)

                    loginWithEmailAndPassword(email, password) {
                        cy.intercept('POST', '**/login').as('postLogin') // Intercept POST
                        cy.contains('#root', 'WELCOME BACK').find('form').then(form => {
                            cy.wrap(form).find('[type="email"]').type(email)
                            cy.wrap(form).find('[type="password"]').type(password)
                            // cy.wrap(form).find('[type="checkbox"]').check({ force: true })
                            cy.wrap(form).submit()
                        })
                        cy.wait('@postLogin').its('response.statusCode')
                            .should('be.oneOf', [200]) // Verify Status code
                    }

        - **commands.js** (This file contains commands for starting different environment and to store .json file from fixture folder before test start)

                    Cypress.Commands.add('openstageUrl', () => {
                        cy.visit(Cypress.env('stageUrl'))
                        cy.fixture('users').then(function (users) {
                            this.users = users
                        })
                        cy.waitUntil(() => cy.title().should('include', 'Site Name'))
                    })

      - **index.js** (This file is processed and loaded automatically before your test files. Global configuration. Behavior that modifies Cypress)
      - **videos** (This folder contains videos that will be saved after test execution)
      - **node_modules** (This is the folder where NPM installs all the project dependencies.)
      - **cypress.env.json** (This file contains different development stages)
            devUrl: https//develop.xxxxx.date
            stageUrl: https://staging.xxxxx.date
      - **cypress.json** (This file contains different Cypress configurations options. E.g., timeout, base URL, test files, or any other configuration.)

            "chromeWebSecurity": false
            "defaultCommandTimeout": 15000`
            "requestTimeout": 15000
            "screenshotsFolder": "cypress/reports/mochareports/assets"

      - **e2e-run-tests.js** (This file contains run commands)

            cypress.run()
            spec: "test.spec.js"

      - **package.json** (This file contains scripts, and dependencies used in our scripts)

          scripts: (We use these commands to run scripts from the terminal or commander)

                "scripts": {
                    “cypress:open”: “node_modules\\.bin\\cypress open”
                    “cypress:run”: “cypress run —headless —browser chrome”
                }
          devDependencies:

                cypress: 6.9.1
                cypress-file-upload: 5.0.6
                cypress-iframe: 1.0.1
                cypress-multi-reporters: 1.5.0
                cypress-wait-until: 1.7.1
                cy-mobile-commands: 0.2.1
                mocha: 8.3.2
                mochawesome: 6.2.2
                mochawesome-merge: 4.2.0
                mochawesome-report-generator: 5.2.0
                moment: 2.29.1

      - **package-lock.json**

![Main Logo](../../assets/logo-furman(50).png){ align=left }

## 06. Additional Details

- **Fixtures**

are external pieces of static data that can be used by your tests. We should not hard code data in the test case. It should drive from an external source like CSV, HTML, or JSON. They will be majorly used with the cy.fixture() command when you need to stub the network calls.

- **Integration**

folder provides a place that writes out test cases. It also provides an “examples” directory, which contains the default test cases provided by Cypress and can be used to add new test cases also. We can also create our folder under the integration directory and add out test cases under that.

- **Plugins**

contain the plugins or listeners. By default, Cypress will automatically include the plugins file “cypress/plugins/index.js” before every test it runs. You can programmatically alter the resolved configuration and environment variables using plugins, Eg. If we have to inject customized options to browsers like accepting the certificate, or do any activity on test case pass or fail or to handle any other events like handling screenshots. They enable you to extend or modify the existing behavior of Cypress.

- **Support**

writes customized commands or reusable methods that are available for usage in all of your spec/test files. This file runs before every single spec file. That’s why you don’t have to import this file in every single one of your spec files.  The “support” file is a great place to put reusable behavior such as Custom Commands or global overrides that you want to be applied and available to all of your spec files.

- **Node_Modules**

in the default project structure is the heart of the cypress project. All the node packages will be installed in the node_modules directory and will be available in all the test files. So, in a nutshell, this is the folder where NPM installs all the project dependencies.

- **Cypress.json**

is used to store different configurations. E.g., timeout, base URL, test files, or any other configuration that we want to override for tweaking the behavior of Cypress. We can also manage the customized folder structure because it is part of by default Cypress Configurations.

![Main Logo](../../assets/logo-furman(50).png){ align=left }

## 07. Basic Actions

- Navigate to the web page you want to test (cy.visit)
- Locate the element of the web page you want to test and perform action (cy.get('button').click())
- Run script from Cypress runner
- Run script with command from terminal and generate report and logs (node_modules\.bin\cypress run --spec "cypress\integration\examples\firsttest.js")
- Navigate to report\mochareports\report.html

![Main Logo](../../assets/logo-furman(50).png){ align=left }

## 08. Useful Links

- [Cypress Documentation](https://docs.cypress.io/)
- [UDEMY - Artem Bondar - Cypress: Web Automation Testing from Zero to Hero](https://www.udemy.com/course/cypress-web-automation-testing-from-zero-to-hero/)
- [Cypress.io](https://www.youtube.com/watch?v=gUFdU5fQs4o&list=PLSRQwlkmpdj6K7pwZmu9v5icbhAiGlQln&ab_channel=Cypress.io)
- [Cypress (Web Testing Framework) Crash Course - 2021](https://www.youtube.com/watch?v=avb-VDa3ZG4&t=2637s&ab_channel=LaithHarb)
- [AutomationStepbyStep - Cypress Beginner Tutorial](https://www.youtube.com/watch?v=CYcdT-tOvB0&list=PLhW3qG5bs-L9LTfxZ5LEBiM1WFfvX3dJo&ab_channel=AutomationStepbyStep)
- [Tips & Tricks for Cypress](https://www.diogonunes.com/blog/cypress-tips-tricks/)
