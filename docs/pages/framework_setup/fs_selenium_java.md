# Java + Selenium + Cucumber + TestProjectIO

## QA Automation Framework Setup

This document is created to enable new employees to get the basic, starting knowledge on how web automation testing works.

### Main Task

Ensure the quality of the products that your team puts in in the hands of customers by automatig web application end users processes.

### Selenium Limitations

- No built-in command for automatic generation of test scripts
- Handling page load or element load is difficult
- Creating test cases is time-consuming
- Difficult to set up test environment as compared to Cypress

### Pre-Requisites

- Install [GIT](https://git-scm.com/) (Version Control System)
- Make sure you have access to [company git repository](https://git.nosolutions.rs/)
- Make sure you have ssh access to your account and key is already added for account.
- Must have account on Test Project cloud platform: [TestProjectIO](https://testproject.io/)
- Run TestProject Agent on your PC (If using [TestProjectIO SDK](https://docs.testproject.io/testproject-sdk/opensdk-v2/java-sdk) instead of chromedriver)
- Install [IntelliJ IDEA](https://www.jetbrains.com/idea/download/#section=windows) (Integrated Development Environment)

## 01. Setup JDK

- Install [JDK 11](https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html) for Windows
- Setup environment variables for Java JDK within your PC
      - Navigate to System Properties **(hit: WinKey, type: env, hit: ENTER):**
      - Navigate to Environment Variables: **`System Properties\Advanced\Environment Variables`**
          - Name: **`JAVA_HOME`**
          - Destination **`C:\Program Files\Java\jdk-11.0.12`**

## 02. Setup MAVEN

- Download [Maven](https://maven.apache.org/download.cgi) (.zip)
- Create Maven destination folder
      - Navigate to **`"C:\Program Files\"`**
      - Create folder **`apache-maven-3.6.3`**
      - Copy .zip file previously downloaded
      - Unzip that file within **`C:\Program Files\apache-maven-3.6.3\`** folder

### Setup Environment variables for Maven

- Navigate to System Properties **(hit: WinKey, type: env, hit: ENTER):**
- Navigate to: **`System Properties\Advanced\Environment Variables`**
      - Within the root set environment variable name and destination:
      - Name: **`%MAVEN_HOME%`**
      - Destination: **`C:\Program Files\apache-maven-3.6.3`**

## 03. Setup NodeJS

- Download [NodeJS 14.17.6](https://nodejs.org/en/blog/release/v14.17.6/)
- Navigate to System Properties **(hit: WinKey, type: env, hit: ENTER):**
- Navigate to: **`System Properties\Advanced\Environment Variables\Path`**
      - Within the root set environment variable name and destination:
      - Name: **`%JAVA_HOME%\bin`**
      - Destination: **`%MAVEN_HOME%\bin`**

## 04. Download Drivers

- Download [Chromedriver](https://chromedriver.chromium.org/downloads)
- Download [Selenium](https://www.selenium.dev/downloads/)

## 05. Create Working Directory

- Open **Command Promt** or **Terminal**
- Navigate to `Documents` folder, type **`cd Documents`** (Or type: **cd Doc** + hit: **Tab**, hit: **Enter**)
- Within **`Documents`** folder type **`mkdir TestingProjects`** (Folder that hold all of your projects)
- Within **`TestingProjects`** folder type **`mkdir SeleniumProject`** (This is your Working directory)

## 06. Clone Project

- Open **Command Promt** or **Terminal**
- Navigate to **`Documents/TestingProjects/SeleniumProject`** folder
- Clone project repository: **`git clone https://git.nosolutions.rs/vladan.furman/onboarding-selenium.git`**
- Open project with **IntelliJ Idea**
- Run **first test** with Maven

### Optional

- Download and install [TestProjectIO](https://testproject.io/)
- Run TestProjectIO Agent
- Setup [TestProjectIO Java SDK](https://www.youtube.com/watch?v=PuwS0FiF9to&list=PLGLlPuoTA3DURLLEpkJJmN7wXgGztfIo3&index=5&t=1068s&ab_channel=UltimateQA)

## 07. Brief Details About The Files And Folders

- **project_name/sauce-demo (project root folder)**
      - **Resources** (This folder contains drivers etc chromedriver, geckodriver...)
      - **src/main/java/com.bwl/** (package folder)
        - **BaseClass.java** (setDriver, driverQuit)
        - **LoginPage.java** (waitForLogoPresence, clickOnLoginButton)
      - **test/java/com.bwl/** (package folder)
        - **StepDefinitions/** (package folder)
          - **Initialize.java** (Before - setDriver, After - driverQuit)
          - **LoginStepDefinitions.java** (Given, Then, When..)
        - **TestRunner** (RunWith - CucumberOptions)
      - **test/resources**
        - **features**
          - **LoginUser.feature** (Cucumber Features and Scenarios: Given, Then, When, Examples)
        - **cucumber.properties**
      - **target** (Cucumber reports...)
        - classes
        - cucumber-reports
        - generated-sources
        - generated-test-sources
        - test-classes
      - **pom.xml** (This file contains all dependencies and plugins)

## 08. Dependencies List (pom.xml)

----

- **build**
      - **plugins**
          - **plugin**

----

- groupId: `org.apache.maven.plugins`
- artifactId: `maven-surefire-plugin`
- version: `3.0.0-M5`
  - configuration:
    - suiteXmlFile: `testng.xml`

----

- groupId: `net.masterhought`
- artifactId: `maven-cucumber-reporting`
- version: `5.3.0`

----

- **dependencies**
      - **dependency**

----

- optional
- groupId: `io.testproject`
- artifactId: `java-sdk`
- version: `1.2.3.-RELEASE`

----

- groupId: `org.seleniumhq.selenium`
- artifactId: `selenium-java`
- version: `3.141.59`

----

- groupId: `org.testng`
- artifactId: `testng`
- version: `7.3.0`

----

- groupId: `io.cucumber`
- artifactId: `cucumber-java`
- version: `6.7.0`

----

- groupId: `io.cucumber`
- artifactId: `cucumber-testng`
- version: `6.7.0`

----

- groupId: `io.cucumber`
- artifactId: `cucumber-core`
- version: `6.7.0`

----

- groupId: `org.junit.jupiter`
- artifactId: `junit-jupiter-api`
- version: `5.5.2`

----

- groupId: `io.cucumber`
- artifactId: `cucumber-junit`
- version: `6.11.0`

----

- **properties**

----

- maven.compiler.source: `11`
- maven.compiler.target: `11`

----

## 09. Basic Actions

- Instantiate a WebDriver object to drive the browser you want to use in your test
- Navigate to the web page you want to test
- Locate the element of the web page you want to test
- Ensure that the browser is in the correct state to interact with that element
- Perform the action on the element
- Record test results
- Create report and logs
- Quit the test

## 10. Useful Links

- [Selenium Webdriver](https://www.selenium.dev/documentation/webdriver/)
- [Selenium-Java GitHub Link](https://github.com/nadvolod/selenium-java)
- [UltimateQA](https://www.youtube.com/watch?v=aIfP8rOx66E&t=1825s&ab_channel=UltimateQA)
