# Python Framework - Selenium WebDriver - HTML Report

## QA Automation Framework Setup

This document is created to enable new employees to get the basic, starting knowledge on how web automation testing works.

### Main Task

Ensure the quality of the products that your team puts in in the hands of customers by automatig web application end users processes.

### Pre-Requisites

- Make sure you have access to git repo `https://git.nosolutions.rs/`
- Make sure you have ssh access to your account and key is already added for account.
- Install [PyCharm](https://www.jetbrains.com/pycharm/)

## 01. Create Working Directory

- Open **Command Promt** or **Terminal**
- Navigate to `Documents` folder, type **`cd Documents`** (Or type: **cd Doc** + hit: **Tab**, hit: **Enter**)
- Within **`Documents`** folder type **`mkdir TestingProjects`** (Folder that hold all of your projects)
- Within **`TestingProjects`** folder type **`mkdir PycharmProjects`** (This is your Working directory)

## 02. Clone Repository

- Within your terminal navigate to: `C:/Users/Username/Documents/TestingProjects/PycharmProjects/`

- Clone project: `git clone https://git.nosolutions.rs/vladan.furman/selenium_python-onboarding.git`

- Open project in PyCharm

## 03. Setup Virtual Environment

- Open **Command Promt** or **Terminal**

- Type: `python -m virtualenv venvs\automations`

- Activate the virtualenv

      - Type: `. .\venvs\automations\bin\activate`

## 04. Install Dependencies

        selenium==3.14.1
        requests==2.23.0
        pytest==6.0.1
        pytest-html==2.0.1
        pytest-xdist==2.5.0
        openpyxl==3.0.9
        pytest-metadata==1.10.0
        PyYAML==5.3.1
        c- lang==4.0.post1
        urllib3==1.25.10
        allure-pytest==2.8.40
        stripe==2.37.2
        pytz==2020.1
        pytest-services==2.2.1

## 05. Install ChromeDriver

- Make sure your version of Chrome is up-to-date, and install the latest Chromedriver for your version from [ChromeDriver](https://chromedriver.chromium.org/)

- Unzip and copy **chromedriver** to
    `C:/Users/username/PycharmProjects/pytestSelenium_project/venv/bin/`

### Install all requirements

- Navigate to: **`/projects/automations/`**
- Type: **`pip install -r test-requirements.txt`**

## 06. Brief Details About The Files And Folders

### Project Structure Tree

- **pytestSelenium_project (root folder)**
  - **Logs** (Used to store execution log files)
  - **pageObjects** (This Python Package folder contains page object file which represents the screens of your web app as a series of objects and encapsulates the features represented by a page.)
  - **Reports** (This folder contains report.html file)
  - **Screenshot** (Saved screenshot for specific scenario)
  - **TestData** (.xml files)
  - **tests** (This Python Package folder contains test_scripts, and conf folder)
    - **/conf/config.ini** (This file is common file and contains common fixtures related to test scripts)
    - **/test_scripts** (This folder contains our actual automation test script "*.py")
  - **utilities** (This Python Package folder contains common utilities functions files)
    - **/readProerties.py** (This file contains common utility functions.)
  - **venvs** (This folder contains all dependencies in our virtual environment)
  - **requirements.txt** (This file represent all dependencies we use) (**pip3 freeze > requirements.txt** to create requirements.txt file)

## 07. Pytest Commands

### Run specific testcase

**`pytest -v -s tests/test_scripts/test_login.py`**

### Run testcase and create html report within "Reports" folder

**`pytest -v -s –html=Reports\report.html tests/test_scripts/test_login.py`**

## 08. Basic Actions

- Instantiate a WebDriver object to drive the browser you want to use in your test
- Navigate to the web page you want to test
- Locate the element of the web page you want to test
- Ensure that the browser is in the correct state to interact with that element
- Perform the action on the element
- Record test results
- Create report and logs
- Quit the test

## 09. Useful Links

- [Selenium Webdriver](https://www.selenium.dev/documentation/webdriver/)
- [TechWithTim - Python Selenium Tutorial](https://www.youtube.com/watch?v=Xjv1sY630Uc&t=189s&ab_channel=TechWithTim)
- [SDET - Selenium with Python - Hybrid Framework Design from scratch](https://www.youtube.com/watch?v=57pjD89IFXA&t=1875s&ab_channel=SDET-QAAutomationTechie)
