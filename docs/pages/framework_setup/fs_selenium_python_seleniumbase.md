# Python - SeleniumBase Framework

## QA Automation Framework Setup

This document is created to enable new employees to get the basic, starting knowledge on how web automation testing works.

### Main Task

Ensure the quality of the products that your team puts in in the hands of customers by automatig web application end users processes.

### Pre-Requisites

- Make sure you have access to git repo: https://git.nosolutions.rs/
- Make sure you have ssh access to your account and key is already added for account.
- Install [Python](https://www.python.org/downloads/)
- Install [PyCharm](https://www.jetbrains.com/pycharm/)

## 01. Create A New Project - PyCharm

- Open **Command Promt** or **Terminal**
- Navigate to `Documents` folder, type **`cd Documents`** (Or type: **cd Doc** + hit: **Tab**, hit: **Enter**)
- Within **`Documents`** folder type **`mkdir TestingProjects`** (Folder that hold all of your projects)
- Within **`TestingProjects`** folder type **`mkdir PyCharmProjects`** (This is your Working directory)

## 02. Clone Project

- Open **Command Promt** or **Terminal**
- Navigate to **`Documents/TestingProjects/PycharmProjects/`** folder
- Clone project repository: **`git clone https://git.nosolutions.rs/vladan.furman/onboarding-seleniumbase.git`**

## 03. Setup Virtual Environment - PyCharm

## 04. Install [SeleniumBaseIO](https://seleniumbase.io/)

- Open **Terminal** and type
      - **``pip3 install seleniumbase``**
      - **``seleniumbase``** (or **``sbase``**) (to check if the installation is passed)

## 05. Install [ChromeDriver](https://chromedriver.chromium.org/)

- Open **Terminal** and type
      - **`sbase install chromedriver latest`** or
      - **`sbase install chromedriver 97.0.4692.71`**

## 06. SeleniumBase Commands

- Run all tests
      - Type **`pytest`** and hit ENTER
- Run specific testcase and generate html report
      - Type **`pytest -k test_home`** and hit ENTER
  
## 07. Brief Details About The Files And Folders

- **pythonSeleniumBase (root folder)**
  - **custom_screenshots** (This folder contains screenshots)
    - **data** (This folder contains data: .png, .jpeg...)
    - **latest_logs** (This folder contains: test_info.txt, page_source_info.html, and screenshot.png)
    - **page_objects** (This Python Package folder contains page object file which represents the screens of your web app as a series of objects and encapsulates the features represented by a page.)
    - **tests** (This Python Package folder contains automation test script)
    - **venvs** (This folder contains all dependencies in our virtual environment)
    - **report.html** (This file is report)
    - **requirements.txt** (This file represent all dependencies we use) (pip3 freeze > requirements.txt (to create req.txt file))

## 08. Useful Links

- [SeleniumBase Documentation](https://seleniumbase.io/)
- [MichaelMintz - SeleniumBase Test Automation Framework Tutorial](https://www.youtube.com/watch?v=Sjzq9kU5kOw&ab_channel=MichaelMintz)
- [Automation Bro - Dilpreet Johal - Selenium Python using SeleniumBase Framework / 3 Hours](https://www.youtube.com/watch?v=D0-QGMacMxA&list=PLGLlPuoTA3DURLLEpkJJmN7wXgGztfIo3&index=5&ab_channel=AutomationBro-DilpreetJohal)