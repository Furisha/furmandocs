# Hello

## My name is Vladan Furman ![Main Logo](../assets/furman_profile_106x209.png){ align=right }

I have been professionally testing software for more than 5 years, and I am passionate about Web Developing, and QA Engineering.

For the last 5 years, I've been working with different software testing technique, and on different International projects. Each of the projects was a differently organized score in every respect, starting with the duration of the projects, testing tools, different environments, operating systems, clients, as well as peoples culture.

I have extensive knowledge of software testing life cycle processes in software development, and I like to share it with colleagues testers.
I like to bring innovations in automation and test averages, and feel good when my knowledge brings benefits to the company that I work for.

## Responsibilities

- Creating tests scripts
- Organization and maintenance of scripts
- Continuous integration process using end-to-end tests depends on frameworks
- Bug reporting and documenting
- Sharing knowledge and actively participating in improvements

## Contact

[LinkedIn](https://www.linkedin.com/in/vladan-furman-317443b4/)

[FurmanDocs](https://furisha.gitlab.io/furmandocs/)