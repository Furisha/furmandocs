#

![Main Logo](assets/logo-furman(300).png){ align=center style="height: 50%; width: 50%; border-radius: 5px;" loading=lazy}

<span style="font-family: roboto; color: #3d669c; font-size: 1.2rem;">This website is an onboarding document with basic informations that is used for faster mastering, comprehension and navigation at the beginning of each project development.</span></br>

<!-- ![Main Logo](assets/logo-furman(300).png){ align=center style="height: 20%; width: 20%; border-radius: 5px;" loading=lazy} -->

<span style="font-family: roboto; color: #5188cf; font-size: 0.8rem;">QA Skills, Selenium, Python, Seleniumbase, TestProjectIO, Coding Guidelines, Links, Tips and Tricks</span></br>
