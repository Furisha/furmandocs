# Welcome to Furman QA Docs

![Main Logo](assets/logo-furman(500).png){ align=left }

Moto:

    if (sad() == true) {
        sad().stop();
        dude_life_is_beautiful(beAwesome);
    }

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

[Subscribe to our newsletter](https://www.google.com/){ .md-button }

![Main Logo](assets/logo-furman(50).png) | ![Main Logo](assets/logo-furman(50).png)  | ![Main Logo](assets/logo-furman(50).png)
------------ | ------------- | ------------
![Main Logo](assets/logo-furman(50).png) | ![Main Logo](assets/logo-furman(50).png)  | ![Main Logo](assets/logo-furman(50).png)
![Main Logo](assets/logo-furman(50).png) | ![Main Logo](assets/logo-furman(50).png)  | ![Main Logo](assets/logo-furman(50).png)

## Commands

- `mkdocs new [dir-name]` - Create a new project.
- `mkdocs serve` - Start the live-reloading docs server.
- `mkdocs build` - Build the documentation site.
- `mkdocs -h` - Print help message and exit.

**Substance** | **Description**
:---: | :---:
**Philosophon** | A unit of logic so tiny only a philosopher could hope to split it.
**Stalinium** | Alloy was used during WW2 providing Soviet forces enough toughness to bounce off enemy projectiles.
**Boyfriend Material** | Material that's getting harder and harder to find these days.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

**The quick brown [fox][1], jumped over the lazy [dog][2].**

[1]: https://en.wikipedia.org/wiki/Fox "Wikipedia: Fox"
[2]: https://en.wikipedia.org/wiki/Dog "Wikipedia: Dog"
